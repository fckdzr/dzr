As 08/2014, the database has 40740944 (41M) track with it last id being 81344000 (81M),
wich mean that the track<=>id mapping is done in a non-contiguous way.
Some range of ids are contigus while some other are stepped by 2 or 10.
To be able to find a track MD5 by it id we must first list all kind of indexing.
This give us the following, where the 2 first digit represent the IDs range, and the last digit is the step between each id.
For example, `10-100-2` will store the `10,12,14,...98,100` ids in a consecutive manner.
The step=0, is a special case is used for very scarse ranges, where indexed MD5 is more efficient.
The format for those files is `struct{uint32 id;uint8 md5[16];}[]`

## hoard data:
```sh
START=580000000;STOP=590000000;STEP=1000
SID="xxx" # use inspector network tab while browsing
TOK="yyy" # use inspector network tab while browsing
for page in $(seq $START $STEP $STOP); do
	IDS=$(seq -s, $page $(($page + $STEP - 1)) )
	curl -s "https://www.deezer.com/ajax/gw-light.php?method=song.getListData&input=3&api_version=1.0&api_token=$TOK" -H "Cookie: sid=$SID" --data-binary '{"sng_ids":['$IDS']}' | jq -r '.results.data[] | .SNG_ID+" "+.MD5_ORIGIN' > $page || echo $page >> fail
done;
```

## convert text to binary:
```bash
alias md5dd='dd conv=notrunc count=1 bs=16 status=none'
for d in * ; do
	set ${d//-/ };
	START=$1; END=$2; STEP=$3;
	for f in $d/*; do
		while read id md5 ; do
			echo $md5 | xxd -r -p | md5dd seek=$((($id-$START)/$STEP)) of=$d.bin
		done < $f
	done
done
```

```py
#!/bin/env python3
import glob
if(len(sys.argv) != 5)return die()
assert len(sys.argv) == 5, """
USAGE: mix.py 400000002 500000002 10 out.bin
"""
start=int(sys.argv[1])
end=int(sys.argv[2])
step=int(sys.argv[3])
out=open(sys.argv[4], "wb")
blob = bytearray(int(((end-start)/step)*16))
for f in glob.glob("*"):
	for line in open(f):
		(idx,md5) = line.rstrip('\n').split(' ')
		idx = int(16 * (int(idx)-start) / step)
		blob[idx:idx+16] = bytes.fromhex(md5)
out.write(blob)
```
